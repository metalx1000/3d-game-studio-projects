function bigcup_event
{	
MY.__BOB = ON;  	// 'bobs' when the player moves	
MY._OFFS_X = 42;   // x,y,z pos of the gun	
MY._OFFS_Y = 20;	
MY._OFFS_Z = 7;	
}

// Desc: shotgun type gun//// uses _WEAPONNUMBER// uses __SILENT

Action bigcup
{
	set my.enable_impact,on; // On impact....
	set my.event bigcup_event; // ...my.event picked up..
}
