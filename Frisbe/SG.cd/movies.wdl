function flesh_vid
{
remove(me);
play_moviefile("mov.avi");
enable_key = off;
enable_mouse = off;
enable_joystick = off;
while (movie_frame != 0) {
// allow interrupting the intro by hitting any key (recommended!!)
//if (key_any != 0) { stop_movie(); }
wait(1);
}
enable_key = on;
enable_mouse = on;
enable_joystick = on;
}

action fris_movie
{
	set my.enable_impact,on; // On impact....
	set my.event flesh_vid; // ...my.event picked up..
	fire();
}