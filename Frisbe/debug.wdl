/////////////////////////////////////////////////////////
// Debug panel - to display fps*10, nexus and error skill
/////////////////////////////////////////////////////////
var fps = 20;
var d_ang[3];
var d_vec[3] = 1, 1, 0;

var wheel;
/////////////////////////////////////////////////////////
PANEL debug_panel
{
	LAYER		3;
	POS_X		0;
	POS_Y		2;
	DIGITS	0,0,3,_a4font,16,fps;

//	DIGITS	20,0,4,_a4font,10,d_vec.x;
//	DIGITS	50,0,4,_a4font,10,d_vec.y;
//	DIGITS	80,0,4,_a4font,10,d_vec.z;
//	DIGITS	110,0,4,_a4font,1,d_ang.x;
//	DIGITS	140,0,4,_a4font,1,d_ang.y;
//	DIGITS	170,0,4,_a4font,1,d_ang.z;
//	DIGITS	50,4,1,_a4font,1,tex_flag4;
//	DIGITS	60,4,1,_a4font,1,tex_flag5;
//	DIGITS	70,4,1,_a4font,1,tex_flag6;

//	DIGITS	20,0,4,_a4font,0.062,total_ticks;	//player.max_x;
//	DIGITS	50,0,4,_a4font,1,hull_fatmax.z;
//	DIGITS	80,0,4,_a4font,1,hull_fatmin.z;

	DIGITS	20,0,4,_a4font,1,wheel;//num_joysticks;//joy_raw.z;	//player.max_x;
	DIGITS	50,0,5,_a4font,1,d_vec;
	DIGITS	80,0,5,_a4font,1,joy2_rot.y;
	DIGITS	120,0,1,_a4font,1,joy_1;
	DIGITS	140,0,1,_a4font,1,joy2_1;
	DIGITS	160,0,1,_a4font,1,joy_3;
	DIGITS	180,0,1,_a4font,1,joy2_3;
	DIGITS	200,0,1,_a4font,1,joy_5;
	DIGITS	220,0,1,_a4font,1,joy_6;
	DIGITS	240,0,1,_a4font,1,joy_7;
	DIGITS	260,0,1,_a4font,1,joy_8;
	DIGITS	280,0,1,_a4font,1,joy_9;

//	DIGITS	50,0,4,_a4font,1,guard._TARGET_X;	//NORMAL.Y;
//	DIGITS	90,0,4,_a4font,1,guard._TARGET_Y;	//debug_val;	//CAMERA.X;
//	DIGITS	120,0,4,_a4font,100,my_floornormal.Z;	//debug_val;	//CAMERA.X;
//	DIGITS	150,0,4,_a4font,1,HIT.Z;
//	DIGITS	180,0,4,_a4font,10,absforce.X;
//	DIGITS	210,0,4,_a4font,10,absforce.Y;
//	DIGITS	240,0,4,_a4font,10,force.X;

//	DIGITS	60,0,4,_a4font,1,ON_PASSABLE_;
//	DIGITS	90,0,4,_a4font,1,IN_PASSABLE_;	//NORMAL.Y;
//	DIGITS	120,0,4,_a4font,1,IN_SOLID_;
//	DIGITS	60,0,5,_a4font,1,temp.X;	//NORMAL.Y;
//	DIGITS	100,0,5,_a4font,1,temp.Y;	//debug_val;	//CAMERA.X;
//	DIGITS	140,0,5,_a4font,1,temp.Z;	//debug_val;	//CAMERA.X;

//	DIGITS	150,0,5,_a4font,1,D3D_TEXSIZE.X;
//	DIGITS	190,0,5,_a4font,1,D3D_TEXSIZE.Y;
//	DIGITS	230,0,5,_a4font,1,D3D_TEXSIZE.Z;

#	DIGITS	130,0,4,_a4font,10,NORMAL.Y;	//debug_val;	//CAMERA.X;
#	DIGITS	170,0,4,_a4font,10,NORMAL.Z;	//debug_val;	//CAMERA.X;

	# Zeitmessung
#	DIGITS	275,0,2,standard_font,1,CD_TRACK;

	FLAGS	= REFRESH,D3D;//,VISIBLE;
}

STRING	debug_labels_1,
	"fps";	// res_x res_y x   y";
#	"fps nx mdist  vx cl slc drw buf ob ac  clp  max  m";

TEXT	debug_text {
	POS_X	= 2;
	POS_Y	= 30;
	FONT	= standard_font;
	STRING = TEX_NAME;	//debug_labels_1;
//	FLAGS = VISIBLE;
}
///////////////////////////////////////////////
function set_debug()
{
	debug_panel.VISIBLE = ON;
	debug_text.VISIBLE = ON;
	WHILE (1) {	// forever
		debug_panel.POS_Y = SCREEN_SIZE.Y - 15;
		debug_text.POS_Y = SCREEN_SIZE.Y - 29;
		fps = 0.9*fps + 0.1*TIME_FAC;
		wheel = 10/(0.1); // += mickey.z;
		d_vec = camera.skyvisible;
		WAIT(1);
	}
}