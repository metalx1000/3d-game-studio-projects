var jentak = 0;

SOUND jenny_talk <jenny.wav>;
SOUND jenny_talk1 <jenny1.wav>;
SOUND jenny_talk2 <jenny2.wav>;
SOUND jenny_talk3 <jenny3.wav>;
SOUND jenny_talk4 <jenny4.wav>;



//////////////////////////////////////////

function jennytalk_event()
{
IF (YOU == PLAYER)
{
My.event=actor_fight;
if (jentak == 0)
{
PLAY_SOUND, jenny_talk, 70;
WAIT 1000;
	MY.ENABLE_TRIGGER = ON;
	MY.TRIGGER_RANGE = 200; // talk range
	MY.EVENT = jennytalk_event;
}
if (jentak == 1)
{
PLAY_SOUND, jenny_talk1, 70;
WAIT 1000;
	MY.ENABLE_TRIGGER = ON;
	MY.TRIGGER_RANGE = 200; // talk range
	MY.EVENT = jennytalk_event;
}
if (jentak == 2)
{
PLAY_SOUND, jenny_talk2, 70;
WAIT 1000;
	MY.ENABLE_TRIGGER = ON;
	MY.TRIGGER_RANGE = 200; // talk range
	MY.EVENT = jennytalk_event;
}
if (jentak == 3)
{
PLAY_SOUND, jenny_talk3, 70;
WAIT 1000;
	MY.ENABLE_TRIGGER = ON;
	MY.TRIGGER_RANGE = 200; // talk range
	MY.EVENT = jennytalk_event;
}
if (jentak == 4)
{
PLAY_SOUND, jenny_talk4, 70;
WAIT 1000;
	MY.ENABLE_TRIGGER = ON;
	MY.TRIGGER_RANGE = 200; // talk range
	MY.EVENT = jennytalk_event;
}
jentak = jentak +1;
if (jentak > 4)
{
jentak = 0;
}
}
}
/////////////////////////////////////







// Desc: Action for jenny
//			Init values and call actor_fight
// 	No WED defined skills
ACTION jenny
{

	MY._FORCE = 2;
	MY._FIREMODE = FIRE_rocket+0.0;
	MY._HITMODE = HIT_GIB;//HIT_EXPLO;
	MY._WALKSOUND = _SOUND_ROBOT;
	actor_fight();
	anim_init();
	drop_shadow();
	MY.ENABLE_TRIGGER = ON;
	MY.TRIGGER_RANGE = 200; // talk range
	MY.EVENT = jennytalk_event;	
	
//	if(MY.FLAG4 == ON) { patrol(); }
//	create(<arrow.pcx>,MY.POS,_ROBOT_TEST_WATCHER);  // used to activate watcher drone
}
}
