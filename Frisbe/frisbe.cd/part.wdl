///Realspawns complete collection of particles//
//Smoke-Fire-Waterbubbles-Fountain-Firesparks-Snow-Waterfall//





// Smoke effect//

bmap smoke_parmap=<smoke.pcx>;

function smoke_effect();	// prototype
function smoke_property();	// prototype

function smoke_effect() {
	my.vel_x=0;
	my.vel_y=0;
	my.vel_z=random(3);
	my.move=on;
	my.size=15;
	my.bmap=smoke_parmap;
	my.flare=on;
	my.alpha=10;
	my.function=smoke_property;
}

function smoke_property() {
	my.alpha-=0.1*time;
	if (my.alpha<=0) {
		my.lifespan=0;
	}
}

action smoke {
	while (1) {
		temp.x=my.x+random(32)-16;
		temp.y=my.y+random(32)-16;
		temp.z=my.z;
		effect(smoke_effect,5*time,temp,temp);
		wait 1;
	}
}


///Fire effect/// 

bmap fire_parmap=<fire.pcx>;

function fire_effect();
function fire_property();


function fire_effect() {
	my.x+=random (30*(sin(total_ticks*200) * cos(total_ticks*50)));

	my.y+=random (30*(sin(total_ticks*200) * sin(total_ticks*50)));

	my.size=15;
	my.vel_x=0;
	my.vel_y=0;
	my.vel_z=2+random(2);
	my.move=on;
	my.bmap=fire_parmap;
	my.flare=on;
	my.bright=on;
	my.alpha=25;
	my.function=fire_property;
}

function fire_property() {
	my.alpha-=1*time;
	if (my.alpha<=0) {
		my.lifespan=0;
	}
}

action fire 
{
	While (1) 
{
		temp.x=my.x;
		temp.y=my.y;
		temp.z=my.z;
		effect(fire_effect,20*time,temp,temp);
		Wait 1;
	}
}


//Snow is falling//


bmap snow_map, <snow.pcx>;
var snow_pos;

action snowfall
{
   my.invisible = on;
   while(1)
   {
       snow_pos.x = my.x + random(256) - 128;
       snow_pos.y = my.y + random(256) - 128;
       snow_pos.z = my.z;
       if (random(1) > 0.8)
       {
           emit(2,snow_pos,particle_snow);
       }
       wait (1);
   }
}

function particle_snow()
{
   if(my_age == 0)
   {  
       my_speed.x = 0;
       my_speed.y = 0;
       my_speed.z = -2.5 + random(1); 
       my_size = 128 + random(64);
       my_map = snow_map;
       my_flare = on;
       my_alpha = 32;
   }
   if(my_age > 128)
   {
       my_action = null;
   }
}

///waterfall//

//Assign to an object in WED. Under the objects properties set skill1 to the life span (fall distance) and
//skill2 to the spray distance (usually pretty close to skill1.

bmap blue_map, <blue1.tga>;
bmap green_map, <rauch.tga>;
var my_life;
var my_spray;
var my_skill1;
var my_skill2;

function water_spray
{
if(my_age == 0)
{
my_speed.y = random(1) - .45;
my_speed.x = random(1) - .75;
my_speed.z = random(1) - 0.5;
my_size = random(150)+50;
my_map = green_map;
my_flare = on;
my_alpha = on;
end;
}

if(my_age > 10)
{
my_alpha = 21 - my_age * 3;} else { 
my_alpha = my_age * 10; }
if(my_age > 20){my_action = null;}
}


function water_fall_particles
{
if(my_age == 0){
my_speed.y = random(1) / 5;
my_speed.z = random(1) - 2; 
my_size = random(250)+550;
my_map = blue_map;
my_flare = on;
my_alpha = on;
end;
}
if (my_age > 40) {
my_alpha = 71 - my_age * 3;
} else { 
my_alpha = my_age * 10; 
}
my_spray = random(2) + 5;
my_life = random(10) + 49;
if(my_age > my_skill2 && my_speed.z < -1.5){{emit(my_spray,my_pos,water_spray);}
if(my_age > my_skill1){my_action = null;}
}

action waterfall
{
my_skill1 = my.skill1;
my_skill2 = my.skill2;
wait(1);
while(1){emit(1,my.pos,water_fall_particles);
wait(1);}
}

//underwaterbubbles////

bmap bub_map, <bub.pcx>;//
var bub_pos;

action bubbles
{
my.invisible = on;
while(1)
{
bub_pos.x = my.x + random(200) - 100;
bub_pos.y = my.y + random(200) - 100;
bub_pos.z = my.z;
if (random(1) > 0.7) // play with this value
{
emit(2,bub_pos,particle_bub);
}
wait (1);
}
}

function particle_bub()
{
if(my_age == 0)
{ 
my_speed.x = 0;
my_speed.y = 0;
my_speed.z = 2.5 + random(1); //this makes it fall "upwards"
my_size = 100 + random(50);
my_map = bub_map;//you need a bubble
my_flare = on;
my_alpha = 30;
}
if(my_age > 250)
{
my_action = null;
}
} 

}

bmap fountain_map = <sparkle.bmp>;
 
function vec_fountain(&vec)
{
vec[0] = random(6) - 3;
vec[1] = random(6) - 3;
vec[2] = random(10) + 20;
}

function particle_alpha_fade()
{
my.alpha -= time * 2;  // stay with framerate
if(my.alpha < 0) { my.lifespan = 0;}
}

function particle_fountain()
{
vec_fountain(temp);
vec_set(my.vel_x,temp); //set the velocity
my.lifespan = 50;
my.flare = on;	//flare transparency
my.bright = on;	//glowing effect
my.move = on;	//move according to vel_x, vel_y, and vel_z
my.streak = on;	//new streak effect
my.bmap = fountain_map;
my.gravity = 3;
my.alpha = 50;
my.size = 10;
my.function = particle_alpha_fade;	//fade out near end of life
}

action fountain
{
	while(1)
	{
		effect(particle_fountain,7 * time,my.x,nullvector);
		wait(1);
	}
}
//set skill2 to the spray distance 

bmap real_map, <fire1.tga>;
bmap spawn_map, <fire2.pcx>;
var my_life;
var my_spray;
var my_skill1;
var my_skill2;

function spark_spray
{
if(my_age == 0)
{
my_speed.y = random(1) - .45;
my_speed.x = random(1) - .75;
my_speed.z = random(1) - 0.5;
my_size = random(150)+50;
my_map = spawn_map;
my_flare = on;
my_alpha = on;
end;
}

if(my_age > 10)
{
my_alpha = 21 - my_age * 3;} else { 
my_alpha = my_age * 10; }
if(my_age > 20){my_action = null;}
}


function spark_fall_particles
{
if(my_age == 0){
my_speed.y = random(1) / 5;
my_speed.z = random(1) - 2; 
my_size = random(250)+550;
my_map = real_map;
my_flare = on;
my_alpha = on;
end;
}
if (my_age > 40) {
my_alpha = 71 - my_age * 3;
} else { 
my_alpha = my_age * 10; 
}
my_spray = random(2) + 40;
my_life = random(10) + 80;
if(my_age > my_skill2 && my_speed.z < -1.5){{emit(my_spray,my_pos,spark_spray);}
if(my_age > my_skill1){my_action = null;}
}

action sparkfall
{
my_skill1 = my.skill1;
my_skill2 = my.skill2;
wait(1);
while(1){emit(1,my.pos,spark_fall_particles);
wait(1);}
}
