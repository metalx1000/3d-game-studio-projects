///////////////////////////////////////////////////////////////////////////////////
// A5 main wdl
////////////////////////////////////////////////////////////////////////////
// Files to over-ride:
// * logodark.bmp - the engine logo, include your game title
// * horizon.pcx - A horizon map displayed over the sky and cloud maps
////////////////////////////////////////////////////////////////////////////
// The PATH keyword gives directories where game files can be found,
// relative to the level directory
path "C:\\PROGRAM FILES\\GSTUDIO\\template";	// Path to WDL templates subdirectory

////////////////////////////////////////////////////////////////////////////
// The INCLUDE keyword can be used to include further WDL files,
// like those in the TEMPLATE subdirectory, with prefabricated actions
include <movement.wdl>;
include <messages.wdl>;
include <menu.wdl>;		// must be inserted before doors and weapons
include <particle.wdl>; // remove when you need no particles
include <doors.wdl>;		// remove when you need no doors
include <actors.wdl>;   // remove when you need no actors
include <weapons.wdl>;  // remove when you need no weapons
include <war.wdl>;      // remove when you need no fighting
//include <venture.wdl>;	// include when doing an adventure
include <lflare.wdl>;   // remove when you need no lens flares
include <chad.wdl>;
include <sounds.wdl>;
include <jenny.wdl>;
include <part.wdl>;

////////////////////////////////////////////////////////////////////////////
// The engine starts in the resolution given by the follwing vars.
var video_mode = 6;	 // screen size 640x480
var video_depth = 16; // 16 bit colour D3D mode

/////////////////////////////////////////////////////////////////
// Strings and filenames
// change this string to your own starting mission message.
string mission_str = "Fight your way through the level. Press [F1] for help";
string level_str = <frisbe.WMB>; // give file names in angular brackets

/////////////////////////////////////////////////////////////////
// define a splash screen with the required A4/A5 logo
bmap splashmap = <logodark.bmp>; // the default logo in templates
panel splashscreen {
	bmap = splashmap;
	flags = refresh,d3d;
}

////////////////////////////////////////////////////////////////////////////
// The following script controls the sky movement
bmap horizon_map = <horizon.pcx>;
// A backdrop texture's horizontal size must be a power of 2;
// the vertical size does not matter

function init_environment()
{
	scene_map = horizon_map;	// horizon backdrop overlay
	scene_nofilter = on;

	sky_speed.x = 1;
	sky_speed.y = 1.5;
	cloud_speed.x = 3;
	cloud_speed.y = 4.5;

	sky_scale = 0.5;
	sky_curve = 1;  			// 'steepness' of sky dome

	scene_field = 60;  		// repeat map 6 times
	scene_angle.tilt = -10; // lower edge of scene_map 10 units below horizon

	sky_clip = scene_angle.tilt;	// clip the sky at bottom of scene_map
}

/////////////////////////////////////////////////////////////////
// The main() function is started at game start
function main()
{
// set some common flags and variables
//	warn_level = 2;	// announce bad texture sizes and bad wdl code
	tex_share = on;	// map entities share their textures

// center the splash screen for non-640x480 resolutions, and display it
	splashscreen.pos_x = (screen_size.x - bmap_width(splashmap))/2;
	splashscreen.pos_y = (screen_size.y - bmap_height(splashmap))/2;
	splashscreen.visible = on;
// wait 3 frames (for triple buffering) until it is flipped to the foreground
	wait(3);

// init the 'environment' (sky & sceen map)
	init_environment();
// now load the level
	level_load(level_str);
// freeze the game
	freeze_mode = 1;

// wait the required second, then switch the splashscreen off.
	waitt(16);
  	splashscreen.visible = off;
	bmap_purge(splashmap);	// remove splashscreen from video memory

// load some global variables, like sound volume
	load_status();

// display the initial message
	msg_show(mission_str,10);

// initialize lens flares when edition supports flares
ifdef CAPS_FLARE;
	lensflare_start();
endif;

// un-freeze the game
	freeze_mode = 0;

//	client_move();	// for a possible multiplayer game
// call further functions here...
}


/////////////////////////////////////////////////////////////////
// The following definitions are for the pro edition window composer
// to define the start and exit window of the application.
WINDOW WINSTART
{
	TITLE			"3D GameStudio";
	SIZE			480,320;
	MODE			IMAGE;	//STANDARD;
	BG_COLOR		RGB(240,240,240);
	FRAME			FTYP1,0,0,480,320;
//	BUTTON		BUTTON_START,SYS_DEFAULT,"Start",400,288,72,24;
	BUTTON		BUTTON_QUIT,SYS_DEFAULT,"Abort",400,288,72,24;
	TEXT_STDOUT	"Arial",RGB(0,0,0),10,10,460,280;
}

/* no exit window at all..
WINDOW WINEND
{
	TITLE			"Finished";
	SIZE			540,320;
	MODE	 		STANDARD;
	BG_COLOR		RGB(0,0,0);
	TEXT_STDOUT	"",RGB(255,40,40),10,20,520,270;

	SET FONT		"",RGB(0,255,255);
	TEXT			"Any key to exit",10,270;
}*/


/////////////////////////////////////////////////////////////////
//INCLUDE <debug.wdl>;

STRING got_bwater_str = "You picked up Brad's Water";

action waterb //brad's water

{
	set my.enable_impact,on; // On impact....
	set my.event pick_up_watbot; // ...my.event picked up..

	while (1)
{
	my.pan += 10 * time; // Spin
	wait (1);
}
}

function pick_up_watbot ()

{
IF (YOU == PLAYER)
{
msg_show(got_bwater_str,10);
remove(me);
}


}

STRING gab_str = "Thank You For Not Littering";

action garb

{
	set my.enable_impact,on; // On impact....
	set my.event gab_throw; // ...my.event picked up..

	
}

function gab_throw()

{
MY.EVENT = NULL;
IF (YOU == PLAYER)
{
msg_show(gab_str,10);
wait 1000;
	set my.enable_impact,on; // On impact....
	set my.event gab_throw; // ...my.event picked up..
}

}


function flesh_vid
{
remove(me);
play_moviefile("mov.avi");
enable_key = off;
enable_mouse = off;
enable_joystick = off;
while (movie_frame != 0) {
// allow interrupting the intro by hitting any key (recommended!!)
//if (key_any != 0) { stop_movie(); }
wait(1);
}
enable_key = on;
enable_mouse = on;
enable_joystick = on;
}

action fris_movie
{
	set my.enable_impact,on; // On impact....
	set my.event flesh_vid; // ...my.event picked up..
	fire();
}




entity* pln;



action plane
{
pln = me;
{	actor_init();	// attach next path	
temp.pan = 360;	
temp.tilt = 180;	
temp.z = 1000;	
result = scan_path(my.x,temp);
	if (result == 0) { my._MOVEMODE = 0; }	// no path found	
// find first waypoint	
ent_waypoint(my._TARGET_X,1);
	while (my._MOVEMODE > 0)	
{		
// find direction
		temp.x = MY._TARGET_X - MY.X;	
	temp.y = MY._TARGET_Y - MY.Y;	
	temp.z = 0;	
	result = vec_to_angle(my_angle,temp);		
force = MY._FORCE;		
// near target? Find next waypoint	
	// compare radius must exceed the turning cycle!
		if (result < 25) { ent_nextpoint(my._TARGET_X); }
		// turn and walk towards target	
	actor_turnto(my_angle.PAN);		
actor_move();		// Wait one tick, then repeat	
	wait(1);	
}
}


action kelly
{
while(1)
{
vec_set(my.x,pln.x);
wait(1);
}
}