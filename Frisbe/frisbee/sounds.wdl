STRING soda_drink = "AAHHH... Nice cool drink.";
STRING stone_house_string = "Ladies and gentalmen. Put your hands together for Stone House Legacy.";

SOUND drink_sound <sodam.wav>;


Action soda_machine()
{
	set my.enable_impact,on; // On impact....
	set my.event drink_soda; // ...my.event picked up..
}

function drink_soda()

{
MY.EVENT = NULL;
IF (YOU == PLAYER)
{
msg_show(soda_drink,10);
PLAY_SOUND, drink_sound, 70;
WAIT 1000;
	set my.enable_impact,on; // On impact....
	set my.event drink_soda; // ...my.event picked up..

}
}




////////////////////////////////////////////////////

SOUND flush_sound <toilet.wav>;

Action toilet()
{
	set my.enable_impact,on; // On impact....
	set my.event flush; // ...my.event picked up..

}

function flush()

{
MY.EVENT = NULL;
IF (YOU == PLAYER)
{
PLAY_SOUND, flush_sound, 70;
WAIT 1000;
set my.enable_impact,on; // On impact....
	set my.event flush; // ...my.event picked up..

}
}



//////////////////////////////////////////////////

function legacy()
{
	MY.EVENT = NULL;
	IF (YOU == PLAYER)
{
	msg_show(stone_house_string,10);
	wait 1000;
}
}


Action stone_house()
{
MY.ENABLE_TRIGGER = ON;
	MY.TRIGGER_RANGE = 200; // talk range
	MY.EVENT = legacy;	
}

