
SOUND chad_talk <chad.wav>;
SOUND chad_talk1 <chad1.wav>;
SOUND chad_talk2 <chad2.wav>;
SOUND chad_talk3 <chad3.wav>;
SOUND chad_talk4 <chad4.wav>;

function chadtalk_event();

function chadtalk4_event()
{
IF (YOU == PLAYER)
{
My.event=actor_fight;
PLAY_SOUND, chad_talk4, 70;
WAIT 1000;
	MY.ENABLE_TRIGGER = ON;
	MY.TRIGGER_RANGE = 200; // talk range
	MY.EVENT = chadtalk_event;
}
}


/////////////////////////////////////////////////

function chadtalk3_event()
{
IF (YOU == PLAYER)
{
My.event=actor_fight;
PLAY_SOUND, chad_talk3, 70;
WAIT 1000;
	MY.ENABLE_TRIGGER = ON;
	MY.TRIGGER_RANGE = 200; // talk range
	MY.EVENT = chadtalk4_event;
}
}

//////////////////////////////////////////////////////
function chadtalk2_event()
{
IF (YOU == PLAYER)
{
My.event=actor_fight;
PLAY_SOUND, chad_talk2, 70;
WAIT 1000;
	MY.ENABLE_TRIGGER = ON;
	MY.TRIGGER_RANGE = 200; // talk range
	MY.EVENT = chadtalk3_event;
}
}

///////////////////////////////////////////////////////

function chadtalk1_event()
{
IF (YOU == PLAYER)
{
My.event=actor_fight;
PLAY_SOUND, chad_talk1, 70;
WAIT 1000;
	MY.ENABLE_TRIGGER = ON;
	MY.TRIGGER_RANGE = 200; // talk range
	MY.EVENT = chadtalk2_event;
}
}

//////////////////////////////////////////

function chadtalk_event()
{
IF (YOU == PLAYER)
{
My.event=actor_fight;
PLAY_SOUND, chad_talk, 70;
WAIT 1000;
	MY.ENABLE_TRIGGER = ON;
	MY.TRIGGER_RANGE = 200; // talk range
	MY.EVENT = chadtalk1_event;
}
}
/////////////////////////////////////







// Desc: Action for Chad
//			Init values and call actor_fight
// 	No WED defined skills
ACTION chad
{

	MY._FORCE = 2;
	MY._FIREMODE = FIRE_rocket+0.0;
	MY._HITMODE = HIT_GIB;//HIT_EXPLO;
	MY._WALKSOUND = _SOUND_ROBOT;
	actor_fight();
	anim_init();
	drop_shadow();
	MY.ENABLE_TRIGGER = ON;
	MY.TRIGGER_RANGE = 200; // talk range
	MY.EVENT = chadtalk_event;	
	
//	if(MY.FLAG4 == ON) { patrol(); }
//	create(<arrow.pcx>,MY.POS,_ROBOT_TEST_WATCHER);  // used to activate watcher drone
}
}
